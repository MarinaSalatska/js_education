/* Создайте массив styles с элементами «Джаз» и «Блюз»*/
const styles = ["Jazz" , "Blues"];
document.write(styles);

/* Добавьте «Рок-н-ролл» в конец*/
const a = styles.concat("Rock-n-roll");
document.write("<p>" + a );

/* Замените значение в середине на «Классика». */
let second = 1;
let res = a.slice(second);
document.write("<p>" + res.join(",Classic,"));

/* Удалите первый элемент массива и покажите его.*/
const d = a.splice(0 , 1);   
document.write("<p>Deleted: " + d);
document.write("<p>Remained: " + a);
debugger

/*Вставьте «Рэп» и «Регги» в начало массива.*/
a.splice(0, 0, "Rep","Reggae",);
document.write("<p>After insertion: " + a);